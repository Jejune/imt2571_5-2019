<?php
/**
  * Class for accessing DOM data representation of the contents of a Disney.xml
  * file
  */
class Disney
{
    /**
      * The object model holding the content of the XML file.
      * @var DOMDocument
      */
    protected $doc;

    /**
      * An XPath object that simplifies the use of XPath for finding nodes.
      * @var DOMXPath
      */
    protected $xpath;

    /**
      * param String $url The URL of the Disney XML file
      */
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
        $this->xpath = new DOMXPath($this->doc);
    }

    /**
      * Creates an array structure listing all actors and the roles they have
      * played in various movies.
      * returns Array The function returns an array of arrays. The keys of they
      *               "outer" associative array are the names of the actors.
      *                The values are numeric arrays where each array lists
      *                key information about the roles that the actor has
      *                played. The elments of the "inner" arrays are string
      *                formatted this way:
      *               'As <role name> in <movie name> (movie year)' - such as:
      *               array(
      *               "Robert Downey Jr." => array(
      *                  "As Tony Stark in Iron Man (2008)",
      *                  "As Tony Stark in Spider-Man: Homecoming (2017)",
      *                  "As Tony Stark in Avengers: Infinity War (2018)",
      *                  "As Tony Stark in Avengers: Endgame (2019)"),
      *               "Terrence Howard" => array(
      *                  "As Rhodey in Iron Man (2008)")
      *               )
      */
    public function getActorStatistics()
    {
        $result = array();
        $tempMArray = array();
        $tempId = '';
        $tempName = '';
        $tempMName = '';
        $tempMYear = '';
        $tempRName = '';
        $tempRes = '';

        $sub1 = $this->xpath->query('Subsidiaries')->item(0);
        $sub2 = $sub1->childNodes;
        $sub3 = $sub2->childNodes;
        $sub4 = $sub3->childNodes[3]->childNodes;
        $actor1 = $this->xpath->query('Actors')->item(0);
        $actor2 = $actor1->childNodes;

        foreach($actor2 as $actor){
          $tempId = $actor->getAttribute('id');
          $tempName = $actor->firstChild;
          foreach($sub2 as $subsidairy){
            if($subsidairy->nodeType == XML_ELEMENT_NODE){
              foreach($sub3 as $movie){
                if($movie->nodeType == XML_ELEMENT_NODE){
                  foreach($sub4 as $role){
                    if($role->getAttribute('actor') == $tempId){
                      $tempMName = $role->parentNode->childNode[0];
                      $tempMYear = $role->parentNode->childNode[1];
                      $tempRName = $role->getAttribute('name');
                      $tempRes = "As ".$tempRName." in ".$tempMName." (".$tempMYear.")";
                      $tempMArray[] = $tempRes;
                    }
                  }
                }
              }
            }
          }
          $result[$tempName] = $tempMArray;
        }
        return $result;
    }

    /**
      * Removes Actor elements from the $doc object for Actors that have not
      * played in any of the movies in $doc - i.e., their id's do not appear
      * in any of the Movie/Cast/Role/@actor attributes in $doc.
      */
    public function removeUnreferencedActors()
    {
      $tempId = '';
      $bool = False;

      $sub1 = $this->xpath->query('Subsidiaries')->item(0);
      $sub2 = $sub1->childNodes;
      $sub3 = $sub2->childNodes;
      $sub4 = $sub3->lastChild->childNodes;
      $actor1 = $this->xpath->query('Actors')->item(0);
      $actor2 = $actor1->childNodes;

      $nodeActors = $this->xpath->query("Actors");
      $nodeRoles = $this->xpath->query("Subsidiary/Subsidiary/Movie/Cast/Role[@actor='$tempId']");

      foreach($actor2 as $actor){
        $tempId = $actor->getAttribute('id');
        foreach($sub2 as $subsidiary){
          foreach($sub3 as $movie){
            foreach($sub4 as $role){
              if($role->getAttribute('actor') == $tempId){
                $bool = True;
              }
            }
          }
        }
        if($bool == False){
          $nodeActors = $this->xpath->query("Actors/Actor[@id='$tempId']");
          if($nodeActors->length > 0){
            $node = $nodeList[0];
            $node->parentNode->removeChild($node);
          }
        }
      }
    }

    /**
      * Adds a new role to a movie in the $doc object.
      * @param String $subsidiaryId The id of the Disney subsidiary
      * @param String $movieName    The name of the movie of the new role
      * @param Integer $movieYear   The production year of the given movie
      * @param String $roleName     The name of the role to be added
      * @param String $roleActor    The id of the actor playing the role
      * @param String $roleAlias    The role's alias (optional)
      */
    public function addRole($subsidiaryId, $movieName, $movieYear, $roleName,
                            $roleActor, $roleAlias = null)
    {
        $sub1 = $this->xpath->query('Subsidiaries')->item(0);
        $sub2 = $sub1->childNodes;
        $sub3 = $sub2->childNodes;
        $sub4 = $sub3->lastChild->childNodes;

        if($sub2->hasAttribute($subsidiaryId)){
          foreach($sub2 as $subsidiary){
            if($subsidiary->getAttribute('id') == $subsidiaryId){
              foreach($sub3 as $movie){
                if($movie->firstChild == $movieName){
                  if($movie->childNodes[1] == $movieYear){
                    foreach($sub4 as $role){
                      if(!($role->hasAttribute($roleName))){
                        $role0 = $this->doc->createElement('Role');
                        $role->appendChild($role0);
                        $role0->setAttribute('name', $roleName);
                        $role0->setAttribute('actor', $roleActor);
                        $role0->setAttribute('alias', $roleAlias);
                      }
                    }
                  }
                }
              }
            }
          }
        }
    }
}
?>
